package com.rocha.kinedutest.injection.component

import com.rocha.kinedutest.activity.ActivitiesListViewModel
import com.rocha.kinedutest.activity.ActivityViewModel
import com.rocha.kinedutest.injection.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(activitiesListViewModel: ActivitiesListViewModel)

    fun inject(activityViewModel: ActivityViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}