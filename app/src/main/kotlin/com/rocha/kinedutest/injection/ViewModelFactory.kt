package com.rocha.kinedutest.injection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.rocha.kinedutest.model.database.AppDatabase
import com.rocha.kinedutest.activity.ActivitiesListViewModel

class ViewModelFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ActivitiesListViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "activities").build()
            @Suppress("UNCHECKED_CAST")
            return ActivitiesListViewModel(db.activityDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}