package com.rocha.kinedutest.network

import com.rocha.kinedutest.model.ActivitiesListData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ActivityApi {

    @GET("/api/v3/catalogue/activities")
    fun getActivities(@Query("skill_id") skillId: Int, @Query("baby_id") babyId: Int): Observable<ActivitiesListData>
}