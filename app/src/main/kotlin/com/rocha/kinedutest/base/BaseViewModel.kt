package com.rocha.kinedutest.base

import androidx.lifecycle.ViewModel
import com.rocha.kinedutest.activity.ActivitiesListViewModel
import com.rocha.kinedutest.activity.ActivityViewModel
import com.rocha.kinedutest.injection.component.DaggerViewModelInjector
import com.rocha.kinedutest.injection.component.ViewModelInjector
import com.rocha.kinedutest.injection.module.NetworkModule

abstract class BaseViewModel : ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector.builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is ActivitiesListViewModel -> injector.inject(this)
            is ActivityViewModel -> injector.inject(this)
        }
    }
}