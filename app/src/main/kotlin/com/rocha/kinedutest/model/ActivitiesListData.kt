package com.rocha.kinedutest.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


data class ActivitiesListData(
    val data: Data,
    val meta: Meta
)

data class Data(
    val id: Int,
    val name: String,
    val type: String,
    val activities: List<Activity>
)

@Entity
data class Activity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val age: Int,
    @Json(name = "age_group")
    val ageGroup: String?,
    @Json(name = "activity_type")
    val activityType: String?,
    @Json(name = "is_holiday")
    val isHoliday: Boolean,
    @Json(name = "domain_id")
    val domainId: Int,
    @Json(name = "area_id")
    val areaId: Int,
    val purpose: String,
    val thumbnail: String,
    @Json(name = "active_milestones")
    val activeMilestones: Int,
    @Json(name = "completed_milestones")
    val completedMilestones: Int
)

data class Meta(
    val page: Int,
    @Json(name = "per_page")
    val perPage: Int,
    @Json(name = "total_pages")
    val totalPages: Int,
    val total: Int
)