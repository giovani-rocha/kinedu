package com.rocha.kinedutest.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ActivityDao {
    @get:Query("SELECT * FROM activity")
    val all: List<Activity>

    @Insert
    fun insertAll(vararg users: Activity)
}