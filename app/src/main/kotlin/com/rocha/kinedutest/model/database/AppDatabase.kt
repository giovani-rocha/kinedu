package com.rocha.kinedutest.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rocha.kinedutest.model.Activity
import com.rocha.kinedutest.model.ActivityDao

@Database(entities = [Activity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun activityDao(): ActivityDao
}