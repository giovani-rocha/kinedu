package com.rocha.kinedutest.activity

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.rocha.kinedutest.R
import com.rocha.kinedutest.base.BaseViewModel
import com.rocha.kinedutest.model.Activity
import com.rocha.kinedutest.model.ActivityDao
import com.rocha.kinedutest.network.ActivityApi
import com.rocha.kinedutest.utils.BABY_ID
import com.rocha.kinedutest.utils.SKILL_ID
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ActivitiesListViewModel(private val activityDao: ActivityDao) : BaseViewModel() {

    @Inject
    lateinit var activityApi: ActivityApi
    val activitiesListAdapter: ActivitiesListAdapter = ActivitiesListAdapter()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadActivities() }

    private lateinit var subscription: Disposable

    init {
        loadActivities()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadActivities() {
        subscription = Observable.fromCallable { activityDao.all }
            .concatMap { dbActivitiesList ->
                if (dbActivitiesList.isEmpty())
                    activityApi.getActivities(SKILL_ID, BABY_ID).concatMap { apiActivityList ->
                        activityDao.insertAll(*apiActivityList.data.activities.toTypedArray())
                        Observable.just(apiActivityList)
                    }
                else
                    Observable.just(dbActivitiesList)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveActivitiesListStart() }
            .doOnTerminate { onRetrieveActivitiesListFinish() }
            .subscribe(
                { result -> onRetrieveActivitiesListSuccess(result as List<Activity>) },
                { onRetrievePostListError() }
            )
    }

    private fun onRetrieveActivitiesListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveActivitiesListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveActivitiesListSuccess(activityList: List<Activity>) {
        activitiesListAdapter.updatePostList(activityList)
    }

    private fun onRetrievePostListError() {
        errorMessage.value = R.string.get_activities_error
    }
}