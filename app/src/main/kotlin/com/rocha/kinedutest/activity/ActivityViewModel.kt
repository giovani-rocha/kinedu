package com.rocha.kinedutest.activity

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.rocha.kinedutest.base.BaseViewModel
import com.rocha.kinedutest.model.Activity
import com.squareup.picasso.Picasso

class ActivityViewModel : BaseViewModel() {

    private val name = MutableLiveData<String>()
    private val purpose = MutableLiveData<String>()
    private val thumbnail = MutableLiveData<String>()

    fun bind(activity: Activity) {
        name.value = activity.name
        purpose.value = activity.purpose
        thumbnail.value = activity.thumbnail
    }

    fun getName(): MutableLiveData<String> {
        return name
    }

    fun getPurpose(): MutableLiveData<String> {
        return purpose
    }

    fun getImageUrl(): MutableLiveData<String> {
        return thumbnail
    }


}

object DataBindingAdapter {
    @BindingAdapter("bind:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: String) {
        Picasso.get()
            .load(imageUrl)
            .placeholder(com.rocha.kinedutest.R.drawable.ic_photo_green_48dp)
            .fit().centerCrop()
            .into(view)
    }
}