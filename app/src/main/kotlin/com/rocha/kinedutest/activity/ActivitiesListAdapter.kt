package com.rocha.kinedutest.activity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rocha.kinedutest.R
import com.rocha.kinedutest.databinding.ItemActivityBinding
import com.rocha.kinedutest.model.Activity

class ActivitiesListAdapter : RecyclerView.Adapter<ActivitiesListAdapter.ViewHolder>() {

    private lateinit var activitiesList: List<Activity>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemActivityBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_activity, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(activitiesList[position])
    }

    override fun getItemCount(): Int {
        return if (::activitiesList.isInitialized) activitiesList.size else 0
    }

    fun updatePostList(activitiesList: List<Activity>) {
        this.activitiesList = activitiesList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemActivityBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = ActivityViewModel()

        fun bind(activity: Activity) {
            viewModel.bind(activity)
            binding.viewModel = viewModel
        }
    }
}